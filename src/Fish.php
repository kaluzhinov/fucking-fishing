<?php

namespace App;

class Fish
{
    private $fishNames = [
        'pike',
        'perch',
        'rubberdick'
    ];
    private string $name;
    public function __construct()
    {
        $this->name = $this->fishNames[array_rand($this->fishNames, 1)];
    }
    public function getName()
    {
        return $this->name;
    }
}
