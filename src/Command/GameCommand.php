<?php

namespace App\Command;

use App\Fish;
use App\KeyPressHelper;
use App\WobblerLure;
use App\SiliconLure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class GameCommand extends Command
{

    protected static $defaultName = 'game';

    protected static $defaultDescription = 'Play in cool game.';

    protected $lures = ['silicon' => SiliconLure::class, 'wobbler' => WobblerLure::class];

    protected function configure(): void
    {
        $this->setHelp('This command allows you to play a game...');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {


        $output->writeln([
            '',
            'Welcome to Fucking Fishing!',
            '',
        ]);

        $helper = $this->getHelper('question');

        $question = new ChoiceQuestion(
            'Choose your favorite lure:',
            array_keys($this->lures)
        );

        $question->setErrorMessage('Lure "%s" is invalid.');

        $lureName = $helper->ask($input, $output, $question);

        $output->writeln("Good job! $lureName is ready to cast! Press space to cast the lure!");

        KeyPressHelper::WaitForKeyPress([" "]);

        $lure = new $this->lures[$lureName];

        do {

            $lure->cast();

            $distance = $lure->getDistance();

            $output->writeln("Piiiiioooooo! Splash! $lureName flew $distance metters away! Press space to start animation or esc to recast.");

            $key = KeyPressHelper::WaitForKeyPress([' ', "\e"]);
        } while ($key != ' ');

        while ($lure->onTheWater()) {

            $lure->animateLure();

            if ($lure->isCatchFish()) {
                $fish = new Fish;
                $output->writeln("\n" . $fish->getName() . " has bitten $lureName while " . $lure->getAnimationName() . "! Landed!!!");
                $lure->setDistance(0);
            }
        }

        if (!isset($fish)) $output->writeln("Got nothing again :( Fucking fishing!");

        return Command::SUCCESS;
    }
}
