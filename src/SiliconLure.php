<?php

namespace App;

class SiliconLure extends Lure implements Animatable
{
    protected int $maxDistance = 40;

    public function __construct()
    {
        if (random_int(1, 2) == 1) $this->setAnimationName('jig');
    }

    public function animateLure(): void
    {
        $this->setDistance($this->getDistance() - 1);
        if ($this->animationName == 'jig') echo "\_/";
        if ($this->animationName == 'regular') echo "-~-";
        usleep(400000);
    }
}
