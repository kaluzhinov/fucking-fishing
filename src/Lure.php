<?php

namespace App;

abstract class Lure
{
    protected int $distance = 0;

    protected int $maxDistance = 30;

    protected string $animationName = 'regular';

    public function onTheWater(): bool
    {
        return ($this->distance > 0);
    }

    public function getDistance(): int
    {
        return $this->distance;
    }

    public function setDistance(int $value)
    {
        $this->distance = $value;
    }

    public function getAnimationName(): string
    {
        return $this->animationName;
    }

    public function setAnimationName(string $value)
    {
        $this->animationName = $value;
    }

    public function cast()
    {
        $this->setDistance(rand(0, $this->maxDistance));
    }

    public function isCatchFish(): bool
    {
        return (random_int(0, 5) == 0);
    }
}
