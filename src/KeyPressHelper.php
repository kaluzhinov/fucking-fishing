<?php

namespace App;

class KeyPressHelper
{

    public static int $MaxPressed = 10;

    public static function WaitForKeyPress(array $keys): ?string
    {
        system("stty -icanon -echo");
        $resSTDIN = fopen("php://stdin", "r");
        $colPress = 0;

        do {
            $colPress += 1;
            $strChar = stream_get_contents($resSTDIN, 1);
        } while (!(in_array($strChar, $keys) || ($colPress > self::$MaxPressed)));

        fclose($resSTDIN);

        if ($colPress == self::$MaxPressed) return null;

        return $strChar;
    }
}
