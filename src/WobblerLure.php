<?php

namespace App;

class WobblerLure extends Lure implements Animatable
{
    public function __construct()
    {
        if (random_int(1, 2) == 1) $this->setAnimationName('twitch');
    }

    public function animateLure(): void
    {
        $this->setDistance($this->getDistance() - 1);
        if ($this->animationName == 'twitch') echo "\|\|";
        if ($this->animationName == 'regular') echo "~-~";
        usleep(400000);
    }
}
