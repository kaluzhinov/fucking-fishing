<?php

namespace App;

interface Animatable
{
    public function animateLure(): void;
}
